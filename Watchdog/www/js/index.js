/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    console.log('Received Device Ready Event');
    console.log('calling setup push');
    // app.setupPush();
    $(function() {
      var deviceUUID;
      var number;

      // if (window.localStorage.getItem("tracknum")) {
      //   trackNumFound();
      // } else {
      //   $("#form").show();
      // }
      $("#btn_change").click(function() {
        var confirmm = confirm("This will erase all the tracking data for this number.");
        if (confirmm == true) {


        }

      });
      $("#btn_submit").click(function() {

        if (isEverythingFilled()) {
          $("#loader").show();
          incId();
        }


      });
      $("#form input, #form select").on("change keyup paste", function() {
        $(this).parent().find(".mdl-textfield__error").css("visibility", "hidden");
      })

      function isEverythingFilled() {
        if (!($("#targetName").val())) {
          $("#targetName").parent().find(".mdl-textfield__error").css("visibility", "visible");
        } else {

          if ($("#countryOptions").val() == "null") {
            $("#countryOptions").parent().find(".mdl-textfield__error").css("visibility", "visible");
          } else {
            if (!($("#NUMBER").val())) {
              $("#NUMBER").parent().find(".mdl-textfield__error").css("visibility", "visible");
            } else {

              return true;

            }
          }
        }
      }

      function removeData() {
        if (window.localStorage.getItem("tracknum") && window.localStorage.getItem("trackid")) {
          $("#loader").show();
          var tracknid = window.localStorage.getItem("trackid");
          console.log(tracknid)
          //tracknid = tracknid.toString();
          numbers.on('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
              var childData = childSnapshot.val();
              if (childData.id == tracknid) {
                childSnapshot.ref.remove();
              }
            });
          });
          users.child(tracknid).remove(function() {
            $("#loader").hide();
            $("#form").show();
            $("#trackingData").hide();
            window.localStorage.removeItem("tracknum");
            window.localStorage.removeItem("trackid");
            $(".onlineData").empty();
            $(".wait-for-user").show();
            lastDataGroup = null;
          });
        }
      }

      function incId() {
        // increment the counter
        counterID.transaction(function(currentValue) {
          return (currentValue || 0) + 1
        }, function(err, committed, ss) {
          if (err) {
            setError(err);
          } else if (committed) {
            // if counter update succeeds, then create record
            // probably want a recourse for failures too
            addRecord(ss.val());
          }
        });
      }

      function addRecord(id) {
        setTimeout(function() {
          countryCode = $("#countryOptions").val();
          numberWitoutCode = $("#NUMBER").val();
          targetName = $("#targetName").val();
          number = "" + countryCode + numberWitoutCode;
          deviceUUID = device.uuid;
          numbers.push({
            id: id,
            MobileNumber: number,
            UUID: deviceUUID,
            targetName: targetName
          }, function() {
            console.log(number);
            window.localStorage.setItem("tracknum", number);
            window.localStorage.setItem("trackid", id);
            window.localStorage.setItem("targetName", targetName);
            var idd = id;
            fetchData();
            trackNumFound();
            users.child(idd).set({
              MobileNumber: number
            })
          });

          // fb.child('records').child('rec' + id).set('record #' + id, function(err) {
          //     err && setError(err);
          // });
        });

      }
      if (window.localStorage.getItem("trackid")) {
      	trackNumFound();
        fetchData();
      } else {
        deviceUUID = device.uuid;
        //console.log(deviceUUID)
        $("#form").hide();
        $("#loader").show();
        $("#trackingData").hide();
        numbers.on('value', function(snapshot) {
        	if(snapshot){
            snapshot.forEach(function(childSnapshot) {
              if (childSnapshot.val().UUID == deviceUUID) {
                window.localStorage.setItem("tracknum", childSnapshot.val().MobileNumber);
                window.localStorage.setItem("trackid", childSnapshot.val().id);
                window.localStorage.setItem("targetName", childSnapshot.val().targetName);
                lastDataGroup = null;
                fetchData();
                trackNumFound();
              }
              else {

                $("#form").show();
                $("#loader").hide();
                $("#trackingData").hide();
              }
            });
        }
        else {

                $("#form").show();
                $("#loader").hide();
                $("#trackingData").hide();
              }
        });

}
      var onlinetime;
      var offlinetime;
      var onlineDate;
      var offlineDate;
      var lastSeenDate;
      var lastSeenTime;
      var updatelastseenInt;
      var lastDataGroup = null;

      function fetchData() {
        users.child(window.localStorage.getItem("trackid")).child("records").on("child_added", function(data, prevChildKey) {
          //console.log(data.val().datatime.split(" ")[0].replace(",", ""))
          var dataArray = data.val().datatime.split(" ");
          var dataDate = dataArray[0].replace(",", "");
          var dataTime = dataArray[1] + " " + dataArray[2];
          if (data.val().status == true) {
            onlineDate = dataDate;
            if (dataDate != lastDataGroup) {
              $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
            }
            cordova.plugins.notification.local.schedule({
						    title: document.localStorage.getItem("targetName") + " is now online.",
						    text: 'Thats pretty easy...',
						    foreground: true
						});
            $(".wait-for-user").hide();
            $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time"></span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
            $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .online-details .date-time").append(dataTime);
            $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").show();
            //clearInterval(updatelastseenInt);
            $("#lastSeen").empty().append("Online");
            onlinetime = data.val().datatime + "";
            if (updatelastseenInt) { clearInterval(updatelastseenInt) };
          } else {
            if (onlineDate) {
              offlineDate = dataDate;
              if (dataDate != lastDataGroup) {
                $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
              }
              if (onlineDate != offlineDate) {
                $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time">--</span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
                $(".onlineData > .data-group[data-date='" + onlineDate + "'] div.data:nth-child(2) .offline-details .current-online").empty().append("--");

              }
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .offline-time").show();
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").hide();
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .date-time").append(dataTime);
              lastSeenDate = dataDate;
              lastSeenTime = dataTime;
              // var onlineTime =
              updatelastseen();

              $("#lastSeen").empty().append(huminized(dataDate) + ", " + dataTime);
              offlinetime = data.val().datatime + "";
              $(".onlineData > .data-group[data-date='" + dataDate + "'] .data:nth-child(2) .online-duration").append(calculateOnlineTime(onlinetime, offlinetime))
            }


          }
          lastDataGroup = dataDate;
          console.log(data.val())
        });
      }

      function updatelastseen() {
        updatelastseenInt = setInterval(function() {
          if (lastSeenDate) {
            $("#lastSeen").empty().append(huminized(lastSeenDate) + ", " + lastSeenTime);
          }
        }, 1000);
      }

      // setInterval(function(){
      // 	var groupheadingL = $(".group-heading").length;
      // 	for(i=0;i<groupheadingL;i++){
      // 		var dataDate = $(".groupx-heading").eq(i).text();
      // 		$(".group-heading").eq(i).empty().append(huminized(dataDate));
      // 	}
      // },1000)


      function trackNumFound() {
        $("#form").hide();
        $("#loader").hide();
        $("#trackingData").show();
        $("#targetNameDisplay").empty().append(window.localStorage.getItem("targetName"));
        $(".tracking-text .tracking-num").empty().append(window.localStorage.getItem("tracknum"));
      }
      // setInterval(function() {
      //   $.ajax({
      //     url: "http://azharuddin8898.0fees.us/whatseen/getnumbers.php",
      //     type: "GET",
      //     dataType: 'jsonp',
      //     success: function(data) {

      //       console.log(data)
      //       // $.each(obj, function(index, value){
      //       //   alert(index + " : " + value);
      //       // })
      //     }

      //   });
      // }, 1000);
      var dialogButton = document.querySelector('.dialog-button');
      var dialog = document.querySelector('#dialog');
      if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
      }
      dialogButton.addEventListener('click', function() {
        dialog.showModal();
      });
      dialog.querySelector('#closeNo')
        .addEventListener('click', function() {
          dialog.close();
        });
      dialog.querySelector('#closeYes')
        .addEventListener('click', function() {
          removeData();
          dialog.close();
        });

      function calculateOnlineTime(onlineTime, offlineTime) {


        var startTime = moment(onlineTime, "MM/DD/YYYY, HH:mm:ss A");
        var endTime = moment(offlineTime, "MM/DD/YYYY, HH:mm:ss A");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) - hours * 60;
        var seconds = parseInt(duration.asSeconds()) - minutes * 60 - hours * 60 * 60;
        var onlineDiration = "";
        if (hours != 0) {
          onlineDiration += hours + (hours > 1 ? (minutes != 0 ? " hours, " : " hours and ") : (minutes != 0 ? " hour, " : " hour and "))
        }
        if (minutes != 0) {
          onlineDiration += minutes + (minutes > 1 ? (seconds != 0 ? " minutes and " : " minutes") : (seconds != 0 ? " minute and " : " minute"))
        }

        if (seconds != 0) {
          onlineDiration += "" + seconds + (seconds > 1 ? " seconds" : " second")
        }
        return onlineDiration;
      }

      function huminized(date) {
        var date = moment(date, "MM/DD/YYYY");
        return date.calendar(null, { sameDay: '[Today]', nextDay: '[Tomorrow]', nextWeek: 'dddd', lastDay: '[Yesterday]', lastWeek: 'DD MMMM, YYYY', sameElse: 'DD MMMM, YYYY' });
      }
    });

  },
  setupPush: function() {
    console.log('calling push init');
    var push = PushNotification.init({
      "android": {
        "senderID": "XXXXXXXX"
      },
      "browser": {},
      "ios": {
        "sound": true,
        "vibration": true,
        "badge": true
      },
      "windows": {}
    });
    console.log('after init');

    push.on('registration', function(data) {
      console.log('registration event: ' + data.registrationId);

      var oldRegId = window.localStorage.getItem('registrationId');
      if (oldRegId !== data.registrationId) {
        // Save new registration ID
        window.localStorage.setItem('registrationId', data.registrationId);
        // Post registrationId to your app server as the value has changed
      }

      var parentElement = document.getElementById('registration');
      var listeningElement = parentElement.querySelector('.waiting');
      var receivedElement = parentElement.querySelector('.received');

      listeningElement.setAttribute('style', 'display:none;');
      receivedElement.setAttribute('style', 'display:block;');
    });

    push.on('error', function(e) {
      console.log("push error = " + e.message);
    });

    push.on('notification', function(data) {
      console.log('notification event');
      navigator.notification.alert(
        data.message, // message
        null, // callback
        data.title, // title
        'Ok' // buttonName
      );
    });
  }
};
