$(function() {
  var number;
  if (localStorage.getItem("tracknum")) {
    trackNumFound();
  } else {
    $("#form").show();
  }
  $("#btn_change").click(function() {
    var confirmm = confirm("This will erase all the tracking data for this number.");
    if (confirmm == true) {


    }

  });
  $("#btn_submit").click(function() {

    if (isEverythingFilled()) {
      $("#loader").show();
      incId();
    }


  });
  $("#form input, #form select").on("change keyup paste", function() {
    $(this).parent().find(".mdl-textfield__error").css("visibility", "hidden");
  })

  function isEverythingFilled() {
    if (!($("#targetName").val())) {
      $("#targetName").parent().find(".mdl-textfield__error").css("visibility", "visible");
    } else {

      if ($("#countryOptions").val() == "null") {
        $("#countryOptions").parent().find(".mdl-textfield__error").css("visibility", "visible");
      } else {
        if (!($("#NUMBER").val())) {
          $("#NUMBER").parent().find(".mdl-textfield__error").css("visibility", "visible");
        } else {

          return true;

        }
      }
    }
  }

  function removeData() {
    if (localStorage.getItem("tracknum") && localStorage.getItem("trackid")) {
      $("#loader").show();
      var tracknid = localStorage.getItem("trackid");
      console.log(tracknid)
      //tracknid = tracknid.toString();
      numbers.on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var childData = childSnapshot.val();
          if (childData.id == tracknid) {
            childSnapshot.ref.remove();
          }
        });
      });
      users.child(tracknid).remove(function() {
        $("#loader").hide();
        $("#form").show();
        $("#trackingData").hide();
        localStorage.removeItem("tracknum");
        localStorage.removeItem("trackid");
        $(".onlineData").empty();
        $(".wait-for-user").show();
        lastDataGroup = null;
      });
    }
  }

  function incId() {
    // increment the counter
    counterID.transaction(function(currentValue) {
      return (currentValue || 0) + 1
    }, function(err, committed, ss) {
      if (err) {
        setError(err);
      } else if (committed) {
        // if counter update succeeds, then create record
        // probably want a recourse for failures too
        addRecord(ss.val());
      }
    });
  }

  function addRecord(id) {
    setTimeout(function() {
      countryCode = $("#countryOptions").val();
      numberWitoutCode = $("#NUMBER").val();

      number = "" + countryCode + numberWitoutCode;

      numbers.push({
        id: id,
        MobileNumber: number
      }, function() {
        console.log(number);
        localStorage.setItem("tracknum", number);
        localStorage.setItem("trackid", id);
        localStorage.setItem("targetName", $("#targetName").val());
        var idd = id;
        fetchData();
        trackNumFound();
        users.child(idd).set({
          MobileNumber: number
        })
      });

      // fb.child('records').child('rec' + id).set('record #' + id, function(err) {
      //     err && setError(err);
      // });
    });

  }
  if (localStorage.getItem("trackid")) {
    fetchData();
  }
  var onlinetime;
  var offlinetime;
  var onlineDate;
  var offlineDate;
  var lastSeenDate;
  var lastSeenTime;
  var updatelastseenInt;
  var lastDataGroup = null;

  function fetchData() {
    users.child(localStorage.getItem("trackid")).child("records").on("child_added", function(data, prevChildKey) {
      //console.log(data.val().datatime.split(" ")[0].replace(",", ""))
      var dataArray = data.val().datatime.split(" ");
      var dataDate = dataArray[0].replace(",", "");
      var dataTime = dataArray[1] + " " + dataArray[2];
      if (data.val().status == true) {
        onlineDate = dataDate;
        if (dataDate != lastDataGroup) {
          $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
        }

        $(".wait-for-user").hide();
        $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time"></span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
        $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .online-details .date-time").append(dataTime);
        $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").show();
        //clearInterval(updatelastseenInt);
        $("#lastSeen").empty().append("Online");
        onlinetime = data.val().datatime + "";
        if (updatelastseenInt) { clearInterval(updatelastseenInt) };
      } else {
        if (onlineDate) {
          offlineDate = dataDate;
          if (dataDate != lastDataGroup) {
            $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
          }
          if (onlineDate != offlineDate) {
            $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time">--</span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
            $(".onlineData > .data-group[data-date='" + onlineDate + "'] div.data:nth-child(2) .offline-details .current-online").empty().append("--");

          }
          $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .offline-time").show();
          $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").hide();
          $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .date-time").append(dataTime);
          lastSeenDate = dataDate;
          lastSeenTime = dataTime;
          // var onlineTime =
          updatelastseen();

          $("#lastSeen").empty().append(huminized(dataDate) + ", " + dataTime);
          offlinetime = data.val().datatime + "";
          $(".onlineData > .data-group[data-date='" + dataDate + "'] .data:nth-child(2) .online-duration").append(calculateOnlineTime(onlinetime, offlinetime))
        }


      }
      lastDataGroup = dataDate;
      console.log(data.val())
    });
  }

  function updatelastseen() {
    updatelastseenInt = setInterval(function() {
      if (lastSeenDate) {
        $("#lastSeen").empty().append(huminized(lastSeenDate) + ", " + lastSeenTime);
      }
    }, 1000);
  }

  // setInterval(function(){
  // 	var groupheadingL = $(".group-heading").length;
  // 	for(i=0;i<groupheadingL;i++){
  // 		var dataDate = $(".groupx-heading").eq(i).text();
  // 		$(".group-heading").eq(i).empty().append(huminized(dataDate));
  // 	}
  // },1000)


  function trackNumFound() {
    $("#form").hide();
    $("#loader").hide();
    $("#trackingData").show();
    $("#targetNameDisplay").empty().append(localStorage.getItem("targetName"));
    $(".tracking-text .tracking-num").empty().append(localStorage.getItem("tracknum"));
  }
  // setInterval(function() {
  //   $.ajax({
  //     url: "http://azharuddin8898.0fees.us/whatseen/getnumbers.php",
  //     type: "GET",
  //     dataType: 'jsonp',
  //     success: function(data) {

  //       console.log(data)
  //       // $.each(obj, function(index, value){
  //       //   alert(index + " : " + value);
  //       // })
  //     }

  //   });
  // }, 1000);
  var dialogButton = document.querySelector('.dialog-button');
  var dialog = document.querySelector('#dialog');
  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
  }
  dialogButton.addEventListener('click', function() {
    dialog.showModal();
  });
  dialog.querySelector('#closeNo')
    .addEventListener('click', function() {
      dialog.close();
    });
  dialog.querySelector('#closeYes')
    .addEventListener('click', function() {
      removeData();
      dialog.close();
    });

  function calculateOnlineTime(onlineTime, offlineTime) {


    var startTime = moment(onlineTime, "MM/DD/YYYY, HH:mm:ss A");
    var endTime = moment(offlineTime, "MM/DD/YYYY, HH:mm:ss A");
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes()) - hours * 60;
    var seconds = parseInt(duration.asSeconds()) - minutes * 60 - hours * 60 * 60;
    var onlineDiration = "";
    if (hours != 0) {
      onlineDiration += hours + (hours > 1 ? (minutes != 0 ? " hours, " : " hours and ") : (minutes != 0 ? " hour, " : " hour and "))
    }
    if (minutes != 0) {
      onlineDiration += minutes + (minutes > 1 ? (seconds != 0 ? " minutes and " : " minutes") : (seconds != 0 ? " minute and " : " minute"))
    }

    if (seconds != 0) {
      onlineDiration += "" + seconds + (seconds > 1 ? " seconds" : " second")
    }
    return onlineDiration;
  }

  function huminized(date) {
    var date = moment(date, "MM/DD/YYYY");
    return date.calendar(null, { sameDay: '[Today]', nextDay: '[Tomorrow]', nextWeek: 'dddd', lastDay: '[Yesterday]', lastWeek: 'DD MMMM, YYYY', sameElse: 'DD MMMM, YYYY' });
  }
});
